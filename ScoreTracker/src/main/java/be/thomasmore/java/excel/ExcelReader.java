/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.excel;

import be.thomasmore.java.entity.Student;
import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.entity.Test;
import java.io.FileInputStream;
import java.util.List;

/**
 *
 * @author Thomas
 */
public interface ExcelReader {
    public List<Student> readStudents(FileInputStream file);
    
    
    public Test readTest(FileInputStream file);
    public List<StudentTest> readStudentTesten(FileInputStream file);
    public Klas readKlas(FileInputStream file);
}
