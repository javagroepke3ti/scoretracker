/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.excel;

import be.thomasmore.java.beans.StudentService;
import be.thomasmore.java.beans.TestService;
import be.thomasmore.java.beans.VakService;
import be.thomasmore.java.entity.Student;
import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.entity.Test;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author e_for
 */
public class ExcelReaderImpl implements ExcelReader {

    
    @EJB
    private VakService vakService;
    @EJB
    private StudentService studentService;
    @EJB
    private TestService testService;
    
    @Override
    public Klas readKlas(FileInputStream file){
        Klas klas = new Klas();
        List<Student> studenten = new ArrayList<Student>();
        String klasnaam = "";
        
        try{
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Student student = new Student();
                
                if (teller > 0)
                {
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                        //Indien in de eerste kolom, moet de waarde in het attribuut lastName
                        case 0:
                            student.setStudentNummer(cell.getStringCellValue());
                            break;
                        //Indien in de tweede kolom, moet de waarde in het attribuut firstName
                        case 1:
                            student.setNaam(cell.getStringCellValue());
                            break;
                        //Indien in de derde kolom, moet de waarde in het attribuut email 
                        case 2:
                            student.setVoornaam(cell.getStringCellValue());
                            break;
                        case 3:
                            student.setEmail(cell.getStringCellValue());
                            break;
                        case 4:
                            klasnaam = cell.getStringCellValue();
                            break;
                        }
                    }
                    
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studenten.add(student);
                    
                }
                
                teller++;
                
            }
            klas.setNaam(klasnaam);
            klas.setStudenten(studenten);
            
            workbook.close();
            file.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return klas;
    }
    @Override
    public List<Student> readStudents(FileInputStream file){
        List<Student>studenten = new ArrayList<Student>();
        
        try {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Student student = new Student();
                
                if (teller > 0)
                {
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                        //Indien in de eerste kolom, moet de waarde in het attribuut lastName
                        case 0:
                            student.setNaam(cell.getStringCellValue());
                            break;
                        //Indien in de tweede kolom, moet de waarde in het attribuut firstName
                        case 1:
                            student.setVoornaam(cell.getStringCellValue());
                            break;
                        //Indien in de derde kolom, moet de waarde in het attribuut email 
                        case 2:
                            student.setEmail(cell.getStringCellValue());
                            break;
                        }
                    }
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studenten.add(student);
                }
                
                teller++;
                
            }
            workbook.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return studenten;
    }    
    
    
    public Test readTest(FileInputStream file){
        Test test = new Test();
        List<StudentTest>studentTesten = new ArrayList<StudentTest>();
        String testnaam;
        
        
        try {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                StudentTest studenttest = new StudentTest();
                
                if (teller > 0)
                {
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                        //Indien in de eerste kolom, moet de waarde in het attribuut naam
                        case 2:
                            
                            studenttest.setStudent(studentService.findStudentByNaam(cell.getStringCellValue()));
                            break;
                        //Indien in de tweede kolom, moet de id van de waarde in het attribuut vak
                        
                        case 3:
                            studenttest.setScore(Double.parseDouble(cell.getStringCellValue()));
                            break;
                        case 4:
                            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                            Date date;
                        try {
                            date = format.parse(cell.getStringCellValue());
                            studenttest.setDatum(date);
                            break;
                        
                        } catch (ParseException ex) {
                            
                        }
                        case 0:
                            testnaam = cell.getStringCellValue();
                            test.setNaam(testnaam);
                            
                            break;
                        case 1:
                            
                            test.setVak(vakService.findVakByNaam(cell.getStringCellValue()));
                        break;
                            
                          
                        
                    }
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studentTesten.add(studenttest);
                }}
                
                teller++;
                
            }
            test.setStudentTesten(studentTesten);
            
            workbook.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return test;
      
    
    
}

public List<StudentTest> readStudentTesten(FileInputStream file){
        List<StudentTest> studenttesten = new ArrayList<StudentTest>();
        
        try {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                StudentTest studenttest = new StudentTest();
                
                if (teller > 0)
                {
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                        //Indien in de eerste kolom, moet de waarde in het attribuut naam
                        case 2:
                            studenttest.setStudent(studentService.findStudentByNaam(cell.getStringCellValue()));
                            break;
                        //Indien in de tweede kolom, moet de id van de waarde in het attribuut vak
                        
                            
                            case 3:
                            studenttest.setScore(Double.parseDouble(cell.getStringCellValue()));
                            break;
                            
                            
                    }
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studenttesten.add(studenttest);
                }}
                
                teller++;
                
            }
            workbook.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        
        
        return studenttesten;
      
    
    
}    
        return null;
}
    
    
}
