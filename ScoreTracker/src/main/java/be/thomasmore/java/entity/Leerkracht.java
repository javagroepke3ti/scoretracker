/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "leerkracht")
public class Leerkracht implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    private Long id;
    
    private String naam;
    
    private String voornaam;
    
    private String gebruikersNaam;
    
    private String wachtwoord;
    
    @OneToMany(mappedBy="leerkracht")
    private List<VakLeerkracht> vakLeerkrachten = new ArrayList<VakLeerkracht>();

    public Leerkracht() {
    }

    public Leerkracht(Long id, String naam, String voornaam, String gebruikersNaam, String wachtwoord) {
        this.id = id;
        this.naam = naam;
        this.voornaam = voornaam;
        this.gebruikersNaam = gebruikersNaam;
        this.wachtwoord = wachtwoord;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getGebruikersNaam() {
        return gebruikersNaam;
    }

    public void setGebruikersNaam(String gebruikersNaam) {
        this.gebruikersNaam = gebruikersNaam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public List<VakLeerkracht> getVakLeerkrachten() {
        return vakLeerkrachten;
    }

    public void setVakLeerkrachten(List<VakLeerkracht> vakLeerkrachten) {
        this.vakLeerkrachten = vakLeerkrachten;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leerkracht)) {
            return false;
        }
        Leerkracht other = (Leerkracht) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naam;
    }
    
}
