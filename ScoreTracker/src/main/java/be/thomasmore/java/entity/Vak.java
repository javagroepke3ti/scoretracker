/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "vak")
public class Vak implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    private Long id;
    
    private String naam;
    
    @ManyToMany(mappedBy="vakken")
    private List<Student> studenten = new ArrayList<Student>();
    
    @OneToMany(mappedBy="vak")
    private List<Test> testen = new ArrayList<Test>();
    
    @OneToMany(mappedBy="vak")
    private List<VakLeerkracht> vakLeerkrachten = new ArrayList<VakLeerkracht>();

    public Vak() {
    }

    public Vak(Long id, String naam) {
        this.id = id;
        this.naam = naam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public List<Student> getStudenten() {
        return studenten;
    }

    public void setStudenten(List<Student> studenten) {
        this.studenten = studenten;
    }

    public List<Test> getTesten() {
        return testen;
    }

    public void setTesten(List<Test> testen) {
        this.testen = testen;
    }

    public List<VakLeerkracht> getVakLeerkrachten() {
        return vakLeerkrachten;
    }

    public void setVakLeerkrachten(List<VakLeerkracht> vakLeerkrachten) {
        this.vakLeerkrachten = vakLeerkrachten;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vak)) {
            return false;
        }
        Vak other = (Vak) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return naam;
    }
    
}
