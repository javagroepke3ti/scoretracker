/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author e_for
 */
@Entity
@Table(name = "vak_leerkracht")
public class VakLeerkracht implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Basic(optional = false)
    @NotNull
    private Long id;
    
    @ManyToOne
    private Vak vak;
    
    @ManyToOne
    private Leerkracht leerkracht;
    
    @ManyToMany
    @JoinTable(name="vakLeerkracht_klas")
    private List<Klas> klassen = new ArrayList<Klas>();

    public VakLeerkracht() {
    }

    public VakLeerkracht(Long id, Vak vak, Leerkracht leerkracht) {
        this.id = id;
        this.vak = vak;
        this.leerkracht = leerkracht;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vak getVak() {
        return vak;
    }

    public void setVak(Vak vak) {
        this.vak = vak;
    }

    public Leerkracht getLeerkracht() {
        return leerkracht;
    }

    public void setLeerkracht(Leerkracht leerkracht) {
        this.leerkracht = leerkracht;
    }

    public List<Klas> getKlassen() {
        return klassen;
    }

    public void setKlassen(List<Klas> klassen) {
        this.klassen = klassen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VakLeerkracht)) {
            return false;
        }
        VakLeerkracht other = (VakLeerkracht) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return leerkracht.toString() + " - " + vak.toString();
    }
    
}
