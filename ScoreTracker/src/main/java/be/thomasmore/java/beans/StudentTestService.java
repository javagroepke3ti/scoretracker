/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.excel.ExcelReaderImpl;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.Date;
import javax.ws.rs.POST;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author e_for
 */
@Stateless
@Path("studentTest")
public class StudentTestService {
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    
    public List<StudentTest> readStudentTestenFromFile(FileInputStream file) throws FileNotFoundException {
        List<StudentTest> studenttesten = new ArrayList<StudentTest>();
        
        ExcelReaderImpl instance = new ExcelReaderImpl();
        studenttesten = instance.readStudentTesten(file);
        return studenttesten;
    }
    
    
    
    //Methode om een lijst van studentTesten op te slaan in de db
    public List<StudentTest> saveStudentTestken(List<StudentTest> studentTesten) {
        for (StudentTest studentTest : studentTesten) {
            em.persist(studentTest);
        }
        return studentTesten;
    }
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten en tévens op te slaan in de db
    @POST
    @Path("upload")
    
    public List<StudentTest> readStudentTestenFromFileAndSave(@FormDataParam("file") FileInputStream file) throws FileNotFoundException {
        List<StudentTest> studenttesten = saveStudentTestken(readStudentTestenFromFile(file));
        return studenttesten;
    } 
    
    //Methode om een studentTest uit de db op te halen adv zijn id
    @GET
    @Path("{studentTestId}")
    public StudentTest findStudentTestById(@PathParam("studentTestId") Long studentTestId) {
        return em.find(StudentTest.class, studentTestId);
    }

    //Methode om alle studentTesten uit de db op te halen
    @GET
    @Path("allStudentTesten")
    public List<StudentTest> findAllStudentTesten() {
        List<StudentTest> studentTesten = em.createQuery("select s from StudentTest s order by s.student.naam").getResultList();
        return studentTesten;
    }
    
    @GET
    @Path("studentTestByDate/{studentId}/{datum1}/{datum2}")
    public List<StudentTest> findTestsPerStudentAndDate(@PathParam("studentId") Long studentId, @PathParam("datum1") Date datum1, @PathParam("datum2") Date datum2)
    {
        Query q = em.createQuery("select s from StudentTest s where s.student.id = :id and s.datum between :datum1 and :datum2 order by s.datum");
        q.setParameter("id", studentId);
        q.setParameter("datum1", datum1);
        q.setParameter("datum2", datum2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
    @GET
    @Path("allTestsFromStudentByVakByDate/{studentId}/{vakId}/{datum1}/{datum2})")
    public List<StudentTest> findAllTestsPerStudentByVakAndDate(@PathParam("studentId") Long studentId, @PathParam("vakId") Long vakId, @PathParam("datum1") Date datum1, @PathParam("datum2") Date datum2)
    {
        Query q = em.createQuery("select s from StudentTest s where s.student.id = :studentId and s.test.vak.id = :vakId and s.datum between :datum1 and :datum2 order by s.datum");
        q.setParameter("studentId", studentId);
        q.setParameter("vakId", vakId);
        q.setParameter("datum1", datum1);
        q.setParameter("datum2", datum2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
    @GET
    @Path("allTestsFromKlasByVakByDate/{klasId}/{vakId}/{datum1}/{datum2})")
    public List<StudentTest> findAllTestsFromKlasByVakAndDate(@PathParam("studentId") Long klasId, @PathParam("vakId") Long vakId, @PathParam("datum1") Date datum1, @PathParam("datum2") Date datum2)
    {
        Query q = em.createQuery("select s from StudentTest s where s.student.klas.id = :klasId and s.test.vak.id = :vakId and s.datum between :datum1 and :datum2 order by s.datum");
        q.setParameter("klasId", klasId);
        q.setParameter("vakId", vakId);
        q.setParameter("datum1", datum1);
        q.setParameter("datum2", datum2);
        List<StudentTest> studentTesten = q.getResultList();
        
        return studentTesten;
    }
    
}
