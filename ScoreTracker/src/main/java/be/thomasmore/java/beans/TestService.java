/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Test;
import be.thomasmore.java.excel.ExcelReaderImpl;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author e_for
 */
@Stateless
@Path("test")
public class TestService {
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten
    @POST
    @Path("list")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Test readTestFromFile(FileInputStream file) throws FileNotFoundException {
        Test test = new Test();
        
        ExcelReaderImpl instance = new ExcelReaderImpl();
        test = instance.readTest(file);
        return test;
    }
    
    //Methode om een lijst van testen op te slaan in de db
    @POST
    @Path("saveTesten")
    public List<Test> saveTesten(List<Test> testen) {
        for (Test test : testen) {
            em.persist(test);
            em.flush();           
        }
        return testen;
    }
    
    //Methode om een test uit de db op te halen adv zijn id
    @GET
    @Path("{testId}")
    public Test findTestById(@PathParam("testId") Long testId) {
        return em.find(Test.class, testId);
    }

    //Methode om alle testen uit de db op te halen
    @GET
    @Path("allTesten")
    public List<Test> findAllTesten() {
        List<Test> testen = em.createQuery("select t from Test t order by t.naam").getResultList();
        return testen;
    }
    
    //Methode om een test uit de db op te halen adv zijn naam
     @GET
    @Path("{testNaam}")
     @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Test findTestByNaam(@PathParam("testNaam") String testNaam) {
        return em.find(Test.class, testNaam);
    }
    
}
