/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Vak;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author e_for
 */
@Stateless
@Path("vak")
public class VakService {
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om een lijst van vakken op te slaan in de db
    @POST
    @Path("saveVakken")
    public List<Vak> saveVakken(List<Vak> vakken) {
        for (Vak vak : vakken) {
            em.persist(vak);
        }
        return vakken;
    }
    
    //Methode om een vak uit de db op te halen adv zijn id
    @GET
    @Path("{vakId}")
    public Vak findVakById(@PathParam("vakId") Long vakId) {
        return em.find(Vak.class, vakId);
    }

    //Methode om alle vakken uit de db op te halen
    @GET
    @Path("allVaken")
    public List<Vak> findAllVaken() {
        List<Vak> vakken = em.createQuery("select v from Vak v order by v.naam").getResultList();
        return vakken;
    }
    
    //Methode om een vak uit de db op te halen adv zijn naam
    @GET
    @Path("{vakNaam}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Vak findVakByNaam(@PathParam("vakNaam") String vakNaam) {
        Vak vak = em.find(Vak.class, vakNaam);
        
        return vak;
    }
    
}
