/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.excel.ExcelReaderImpl;
import java.io.FileInputStream;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author e_for
 */
@Stateless
@Path("klas")
public class KlasService {    
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om een lijst van klassen op te slaan in de db
    @POST
    @Path("maakMeerderKlas")
    public List<Klas> saveKlassen(List<Klas> klassen) {
        for (Klas klas : klassen) {
            em.persist(klas);
            em.flush();
        }
        return klassen;
    }
    
    //Methode om één klas toe te voegen aan de db
    @POST
    @Path("maakÉénKlas")
    public Klas addKlas(FileInputStream file){
        Klas klas;
        
        ExcelReaderImpl instance = new ExcelReaderImpl();
        klas = instance.readKlas(file);
        
        em.persist(klas);
        
        return klas;
    }
    
    //Methode om een klas uit de db op te halen adv zijn id
    @GET
    @Path("{klasId}")
    public Klas findKlasById(@PathParam("klasId") Long klasId) {
        return em.find(Klas.class, klasId);
    }

    //Methode om alle klassen uit de db op te halen
    @GET
    @Path("allKlassen")
    public List<Klas> findAllKlassen() {
        List<Klas> klassen = em.createQuery("select k from Klas k order by k.naam").getResultList();
        return klassen;
    }
}
