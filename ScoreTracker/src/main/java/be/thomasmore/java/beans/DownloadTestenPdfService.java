/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.entity.Test;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;

/**
 *
 * @author Thomas
 */
@Stateless
@Path("Testen")
public class DownloadTestenPdfService {

    @EJB
    private TestService testService;
    @EJB
    private KlasService klasService;

    public void createPdf(long klasId, long testId, HttpServletResponse response) throws IOException {
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "attachment; filename=TestVerslag.pdf");
        
        DecimalFormat df = new DecimalFormat("0.00");

        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

        Klas klas = klasService.findKlasById(klasId);
        Test gekozenTest = testService.findTestById(testId);

        double klasGemiddelde = 0;
        double[] scores;
        double mediaan;
        int teller = 0;

        OutputStream pdfFileout = response.getOutputStream();

        PdfWriter writer = new PdfWriter(pdfFileout);

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);

        //Document aanmaken
        Document document = new Document(pdf);
        {

            //String vakNaam =  gekozenTest.getVak().getNaam();
            document.add(new Paragraph(
                    "Klas: " + klas.getNaam() + "\n"
                    + "Vak: " + gekozenTest.getVak() + "\n"
                    + "Onderwerp: " + gekozenTest.getNaam() + "\n"
            ));

            Table table = new Table(new float[]{4, 4});
            table.setBorderTop(new SolidBorder(1));
            table.setBorderBottom(new SolidBorder(1));
            table.setBorderLeft(new SolidBorder(1));
            table.setBorderRight(new SolidBorder(1));
            process(table, "Leerling;Punten;", bold, Color.LIGHT_GRAY);
            table.setWidthPercent(100);
            document.add(new Paragraph());
            List<StudentTest> studentTesten = gekozenTest.getStudentTesten();
            scores = new double[klas.getStudenten().size()];

            //vakken.get(0).getTesten().get(0).getKlassen().get(0).getTesten();
            for (StudentTest studentTest : studentTesten) {
                if (studentTest.getStudent().getKlas().getNaam().equals(klas.getNaam())) {
                    scores[teller] = studentTest.getScore();
                    klasGemiddelde += studentTest.getScore();
                    if (teller % 2 == 0) {
                        process(table, studentTest.getStudent().getVoornaam() + " " + studentTest.getStudent().getNaam() + ";" + df.format(studentTest.getScore()) + "%", font, Color.CYAN);
                    } else {
                        process(table, studentTest.getStudent().getVoornaam() + " " + studentTest.getStudent().getNaam() + ";" + df.format(studentTest.getScore()) + "%", font, Color.WHITE);
                    }
                    teller += 1;
                }
            }
            klasGemiddelde /= scores.length;
            Arrays.sort(scores);
            mediaan = scores[scores.length / 2];
            document.add(new Paragraph("Gemiddelde: " + df.format(klasGemiddelde) + "%" + "\n" + "Mediaan: " + df.format(mediaan) + "%"));

            document.add(table);
            document.close();
        }
        writer.flush();
        writer.close();
    }

    public void process(Table table, String line, PdfFont font, Color backgroundColor) {
        StringTokenizer tokenizer = new StringTokenizer(line, ";");
        while (tokenizer.hasMoreTokens()) {

            table.addCell(
                    new Cell().add(
                            new Paragraph(tokenizer.nextToken()).setFont(font).setMarginLeft(1)).setBorder(Border.NO_BORDER).setPadding(0.5f).setPaddingRight(0.5f).setBorderLeft(new SolidBorder(1)).setBackgroundColor(backgroundColor));
        }
    }
}
