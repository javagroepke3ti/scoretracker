/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Student;
import be.thomasmore.java.excel.ExcelReaderImpl;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataParam;
import javax.persistence.Query;
import org.jvnet.hk2.annotations.Service;

/**
 *
 * @author Thomas
 */
@Service
@Stateless
@Path("student")
@Transactional
public class StudentService{
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten
    @POST
    @Path("list")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<Student> readStudentsFromFile(FileInputStream file) throws FileNotFoundException {
        List<Student> studenten = new ArrayList<Student>();
        
        ExcelReaderImpl instance = new ExcelReaderImpl();
        studenten = instance.readStudents(file);
        return studenten;
    }
    
    //Methode om een lijst van studenten op te slaan in de db
    @POST
    @Path("save")
    public List<Student> saveStudents(List<Student> studenten) {
        for (Student student : studenten) {
            em.persist(student);
        }
        return studenten;
    }
    
    //Methode om een student aan te passen
    @POST
    @Path("update")
    public void updateStudent (Student student)
    {
        Student oud = em.find(Student.class, student.getId());
        
        oud.setNaam(student.getNaam());
        oud.setVoornaam(student.getVoornaam());
        oud.setEmail(student.getEmail());
        oud.setKlas(student.getKlas());
        oud.setTesten(student.getTesten());
        oud.setVakken(student.getVakken());
        
        em.flush();
    }
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten en tévens op te slaan in de db
    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<Student> readStudentsFromFileAndSave(@FormDataParam("file") FileInputStream file) throws FileNotFoundException {
        List<Student> studenten = saveStudents(readStudentsFromFile(file));
        return studenten;
    }
    
    //Methode om een student uit de db op te halen adv zijn id
    @GET
    @Path("{studentId}")
    public Student findStudentById(@PathParam("studentId") Long studentId) {
        return em.find(Student.class, studentId);
    }

    //Methode om alle studenten uit de db op te halen
    @GET
    @Path("allStudents")
    public List<Student> findAllStudents() {
        List<Student> studenten = em.createQuery("select s from Student s order by s.naam").getResultList();
        return studenten;
    }
    
     //Methode om alle studenten van één klas uit de db op te halen
    @GET
    @Path("allStudentsPerKlas/{klasId}")
    public List<Student> findAllStudentsPerKlas(@PathParam("klasId") Long klasId) {
        Query q = em.createQuery("select s from Student s where s.klas.id = :klasId order by s.naam");
        q.setParameter("klasId", klasId);
        List<Student> studenten = q.getResultList();
        return studenten;
    }
    
     //Methode om een student uit de db op te halen adv zijn naam
    @GET
    @Path("{studentNaam}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Student findStudentByNaam(@PathParam("studentNaam") String studentNaam) {
        return em.find(Student.class, studentNaam);
    }

}
