/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.VakLeerkracht;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author e_for
 */
@Stateless
@Path("vakLeerkracht")
public class VakLeerkrachtService {
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om een lijst van vakLeerkrachten op te slaan in de db
    @POST
    @Path("saveVakLeerkrachten")
    public List<VakLeerkracht> saveVakLeerkrachten(List<VakLeerkracht> vakLeerkrachten) {
        for (VakLeerkracht vakLeerkracht : vakLeerkrachten) {
            em.persist(vakLeerkracht);
        }
        return vakLeerkrachten;
    }
    
    //Methode om een vakLeerkracht uit de db op te halen adv zijn id
    @GET
    @Path("{vakLeerkrachtId}")
    public VakLeerkracht findVakLeerkrachtById(@PathParam("vakLeerkrachtId") Long vakLeerkrachtId) {
        return em.find(VakLeerkracht.class, vakLeerkrachtId);
    }

    //Methode om alle vakLeerkrachten uit de db op te halen
    @GET
    @Path("allVakLeerkrachten")
    public List<VakLeerkracht> findAllVakLeerkrachten() {
        List<VakLeerkracht> vakLeerkrachten = em.createQuery("select v from VakLeerkracht v order by v.leerkracht.naam").getResultList();
        return vakLeerkrachten;
    }
}
