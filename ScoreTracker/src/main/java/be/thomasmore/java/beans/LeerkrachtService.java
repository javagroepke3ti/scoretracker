/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.beans;

import be.thomasmore.java.entity.Leerkracht;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author e_for
 */
@Stateless
@Path("leerkracht")
public class LeerkrachtService {
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om een lijst van leerkrachten op te slaan in de db
    @POST
    @Path("saveLeerkrachten")
    public List<Leerkracht> saveLeerkrachten(List<Leerkracht> leerkrachten) {
        for (Leerkracht lk : leerkrachten) {
            em.persist(lk);
        }
        return leerkrachten;
    }
    
    //Methode om een leerkracht uit de db op te halen adv zijn id
    @GET
    @Path("{leerkrachtId}")
    public Leerkracht findLeerkrachtById(@PathParam("leerkrachtId") Long leerkrachtId) {
        return em.find(Leerkracht.class, leerkrachtId);
    }

    //Methode om alle leerkrachten uit de db op te halen
    @GET
    @Path("allLeerkrachten")
    public List<Leerkracht> findAllLeerkrachten() {
        List<Leerkracht> leerkrachten = em.createQuery("select l from Leerkracht l order by l.naam").getResultList();
        return leerkrachten;
    }
}
