/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.beans.KlasService;
import be.thomasmore.java.beans.StudentService;
import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.Student;
import be.thomasmore.java.entity.Test;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author e_for
 */
@WebServlet(name = "KeuzeServlet", urlPatterns = {"/KeuzeServlet"})
public class KeuzeServlet extends HttpServlet {

    @EJB
    private StudentService studentService;
    @EJB
    private KlasService klasService;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = null;
        
        if (request.getParameter("rapportStudent") != null)
        {
            List<Klas> klassen = klasService.findAllKlassen();
            
            request.setAttribute("klassen", klassen);
            request.setAttribute("keuze", "klas");
            rd = request.getRequestDispatcher("rapport_student.jsp");
        }
        
        else if (request.getParameter("toonStudenten") != null)
        {
            if (!request.getParameter("klassen").equals("keuze"))
            {
                Long klasId = Long.parseLong(request.getParameter("klassen"));
                List<Student>studenten = studentService.findAllStudentsPerKlas(klasId);
                request.setAttribute("studenten", studenten);
                request.setAttribute("keuze", "student");
                request.setAttribute("klasId", klasId);
                rd = request.getRequestDispatcher("rapport_student.jsp"); 
            }
            else
            {
                List<Klas> klassen = klasService.findAllKlassen();
            
                request.setAttribute("klassen", klassen);
                request.setAttribute("keuze", "klas");
                request.setAttribute("foutKlas", "Kies een klas!");
                rd = request.getRequestDispatcher("rapport_student.jsp");
            }
        }
        
        else if (request.getParameter("rapportOpdracht") != null)
        {
            List<Klas> klassen = klasService.findAllKlassen();
            
            request.setAttribute("klassen", klassen);
            request.setAttribute("keuze", "klas");
            rd = request.getRequestDispatcher("rapport_opdracht.jsp");
        }
        
        else if (request.getParameter("toonTesten") != null)
        {
            if (!request.getParameter("klassen").equals("keuze"))
            {
                Long klasId = Long.parseLong(request.getParameter("klassen"));
                Klas klas = klasService.findKlasById(klasId);
                List<Test>testen= klas.getTesten();
                request.setAttribute("testen", testen);
                request.setAttribute("keuze", "test");
                request.setAttribute("klasId", klasId);
                rd = request.getRequestDispatcher("rapport_opdracht.jsp"); 
            }
            else
            {
                List<Klas> klassen = klasService.findAllKlassen();
            
                request.setAttribute("klassen", klassen);
                request.setAttribute("keuze", "klas");
                request.setAttribute("foutKlas", "Kies een klas!");
                rd = request.getRequestDispatcher("rapport_opdracht.jsp");
            }
        }
        
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
