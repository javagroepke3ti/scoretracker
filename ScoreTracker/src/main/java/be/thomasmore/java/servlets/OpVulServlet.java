/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.beans.KlasService;
import be.thomasmore.java.beans.LeerkrachtService;
import be.thomasmore.java.beans.StudentService;
import be.thomasmore.java.beans.StudentTestService;
import be.thomasmore.java.beans.TestService;
import be.thomasmore.java.beans.VakLeerkrachtService;
import be.thomasmore.java.beans.VakService;
import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.Leerkracht;
import be.thomasmore.java.entity.Student;
import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.entity.Test;
import be.thomasmore.java.entity.Vak;
import be.thomasmore.java.entity.VakLeerkracht;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author e_for
 */
@WebServlet(name = "OpVulServlet", urlPatterns = {"/OpVulServlet"})
public class OpVulServlet extends HttpServlet {

    @EJB
    private KlasService klasService;
    @EJB
    private StudentService studentService;
    @EJB
    private VakService vakService;
    @EJB
    private LeerkrachtService leerkrachtService;
    @EJB
    private TestService testService;
    @EJB
    private StudentTestService studentTestService;
    @EJB
    private VakLeerkrachtService vakLeerkrachtService;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<Klas>klassen = new ArrayList<>();        
        Klas klas = new Klas();
        klas.setNaam("1A");        
        Klas klas2 = new Klas();
        klas2.setNaam("1B");        
        klassen.add(klas);
        klassen.add(klas2);        
        klasService.saveKlassen(klassen);
        
        List<Leerkracht>leerkrachten = new ArrayList<>();        
        Leerkracht lk = new Leerkracht();
        lk.setNaam("Kelly");
        lk.setVoornaam("Gene");
        lk.setGebruikersNaam("GKelly");
        lk.setWachtwoord("rain");   
        Leerkracht lk2 = new Leerkracht();
        lk2.setNaam("Sinatra");
        lk2.setVoornaam("Frank");
        lk2.setGebruikersNaam("FrankieBoy");
        lk2.setWachtwoord("newyork");        
        Leerkracht lk3 = new Leerkracht();
        lk3.setNaam("Simone");
        lk3.setVoornaam("Nina");
        lk3.setGebruikersNaam("Ninanina");
        lk3.setWachtwoord("raycharles");        
        leerkrachten.add(lk);
        leerkrachten.add(lk2);
        leerkrachten.add(lk3);        
        leerkrachtService.saveLeerkrachten(leerkrachten);
        
        List<Vak>vakken = new ArrayList<>();        
        Vak vak = new Vak();
        vak.setNaam("Linux Advanced");        
        Vak vak2 = new Vak();
        vak2.setNaam("Applicatie-ontwikkeling");        
        Vak vak3 = new Vak();
        vak3.setNaam("Business-project");        
        vakken.add(vak);
        vakken.add(vak2);
        vakken.add(vak3);        
        vakService.saveVakken(vakken); 
        
        List<Test>testen = new ArrayList<>();        
        Test test = new Test();
        test.setNaam("Tutorial3");
        test.setVak(vak);
        List<Klas>klassen1 = new ArrayList<>();
        klassen1.add(klas);
        test.setKlassen(klassen1);        
        Test test2 = new Test();
        test2.setNaam("Database");
        test2.setVak(vak);        
        test2.setKlassen(klassen1);        
        Test test3 = new Test();
        test3.setNaam("BubbleShooter");
        test3.setVak(vak2);        
        test3.setKlassen(klassen);        
        Test test4 = new Test();
        test4.setNaam("PBS");
        test4.setVak(vak3);
        List<Klas>klassen2 = new ArrayList<>();
        klassen2.add(klas2);
        test4.setKlassen(klassen2);   
        Test test5 = new Test();
        test5.setNaam("Space Invaders");
        test5.setVak(vak2);
        test5.setKlassen(klassen);
        testen.add(test);
        testen.add(test2);
        testen.add(test3);
        testen.add(test4);
        testen.add(test5);
        testService.saveTesten(testen);
        
        List<VakLeerkracht> vakLeerkrachten = new ArrayList<>();        
        VakLeerkracht vl = new VakLeerkracht();
        vl.setLeerkracht(lk);
        vl.setVak(vak);
        vl.setKlassen(klassen1);        
        VakLeerkracht vl2 = new VakLeerkracht();
        vl2.setLeerkracht(lk2);
        vl2.setVak(vak2);
        vl2.setKlassen(klassen2);        
        VakLeerkracht vl3 = new VakLeerkracht();
        vl3.setLeerkracht(lk3);
        vl3.setVak(vak3);
        vl3.setKlassen(klassen2);        
        VakLeerkracht vl4 = new VakLeerkracht();
        vl4.setLeerkracht(lk);
        vl4.setVak(vak2);
        vl4.setKlassen(klassen1);        
        vakLeerkrachten.add(vl);
        vakLeerkrachten.add(vl2);
        vakLeerkrachten.add(vl3);
        vakLeerkrachten.add(vl4);        
        vakLeerkrachtService.saveVakLeerkrachten(vakLeerkrachten);
        
        List<Student>studenten = new ArrayList<>();
        Student student = new Student();
        student.setNaam("Lennon");
        student.setVoornaam("John");
        student.setEmail("john@beatles.com");
        student.setKlas(klas);
        student.setStudentNummer("r0853641");        
        List<Vak>vakken1 = new ArrayList<Vak>();
        vakken1.add(vak);
        vakken1.add(vak2);
        student.setVakken(vakken1);
        Student student2 = new Student();
        student2.setNaam("Verstraeten");
        student2.setVoornaam("Danny");
        student2.setEmail("danny@hetnieuws.be");
        student2.setKlas(klas); 
        student2.setStudentNummer("r0703299");
        student2.setVakken(vakken1);
        Student student3 = new Student();
        student3.setNaam("Minogue");
        student3.setVoornaam("Kylie");
        student3.setEmail("km@neighbours.au");
        student3.setKlas(klas2); 
        student3.setStudentNummer("r0884371");
        List<Vak>vakken2 = new ArrayList<Vak>();
        vakken2.add(vak2);
        vakken2.add(vak3);
        student3.setVakken(vakken2);
        Student student4 = new Student();
        student4.setNaam("Potter");
        student4.setVoornaam("Harry");
        student4.setEmail("thechosenone@hogwarts.com");
        student4.setKlas(klas2);
        student4.setVakken(vakken2); 
        student4.setStudentNummer("r0145573");
        Student student5 = new Student();
        student5.setNaam("Collins");
        student5.setVoornaam("Phil");
        student5.setEmail("phil@genesis.com");
        student5.setKlas(klas);
        student5.setStudentNummer("r0722651");    
        student5.setVakken(vakken1);
        Student student6 = new Student();
        student6.setNaam("Kidman");
        student6.setVoornaam("Nicole");
        student6.setEmail("nicole@gmail.com");
        student6.setKlas(klas2);
        student6.setVakken(vakken2); 
        student6.setStudentNummer("r0932284");
        studenten.add(student);
        studenten.add(student2);
        studenten.add(student3);
        studenten.add(student4);  
        studenten.add(student5); 
        studenten.add(student6);
        studentService.saveStudents(studenten);
        
        List<StudentTest>studentTesten = new ArrayList<>();        
        StudentTest st = new StudentTest();
        st.setTest(test);
        st.setStudent(student);
        st.setScore(75.0);             
        StudentTest st2 = new StudentTest();
        st2.setTest(test);
        st2.setStudent(student2);
        st2.setScore(64.8);      
        StudentTest st3 = new StudentTest();
        st3.setTest(test3);
        st3.setStudent(student3);
        st3.setScore(42.8);       
        StudentTest st4 = new StudentTest();
        st4.setTest(test3);
        st4.setStudent(student4);
        st4.setScore(98.0);        
        StudentTest st5 = new StudentTest();
        st5.setTest(test3);
        st5.setStudent(student6);
        st5.setScore(81.9);  
        StudentTest st6 = new StudentTest();
        st6.setTest(test2);
        st6.setStudent(student);
        st6.setScore(64.8);  
        StudentTest st7 = new StudentTest();
        st7.setTest(test2);
        st7.setStudent(student2);
        st7.setScore(78.1); 
        StudentTest st8 = new StudentTest();
        st8.setTest(test2);
        st8.setStudent(student5);
        st8.setScore(64.7); 
        StudentTest st9 = new StudentTest();
        st9.setTest(test);
        st9.setStudent(student5);
        st9.setScore(22.1); 
        StudentTest st10 = new StudentTest();
        st10.setTest(test3);
        st10.setStudent(student);
        st10.setScore(87.1);       
        StudentTest st11 = new StudentTest();
        st11.setTest(test3);
        st11.setStudent(student5);
        st11.setScore(72.0);        
        StudentTest st12 = new StudentTest();
        st12.setTest(test3);
        st12.setStudent(student2);
        st12.setScore(34.1); 
        StudentTest st13 = new StudentTest();
        st13.setTest(test4);
        st13.setStudent(student3);
        st13.setScore(24.8); 
        StudentTest st14 = new StudentTest();
        st14.setTest(test4);
        st14.setStudent(student4);
        st14.setScore(77.0);
        StudentTest st15 = new StudentTest();
        st15.setTest(test4);
        st15.setStudent(student6);
        st15.setScore(59.1); 
        StudentTest st16 = new StudentTest();
        st16.setTest(test5);
        st16.setStudent(student);
        st16.setScore(47.9); 
        StudentTest st17 = new StudentTest();
        st17.setTest(test5);
        st17.setStudent(student2);
        st17.setScore(52.1); 
        StudentTest st18 = new StudentTest();
        st18.setTest(test5);
        st18.setStudent(student5);
        st18.setScore(86.3); 
        StudentTest st19 = new StudentTest();
        st19.setTest(test5);
        st19.setStudent(student3);
        st19.setScore(59.1); 
        StudentTest st20 = new StudentTest();
        st20.setTest(test5);
        st20.setStudent(student4);
        st20.setScore(17.8); 
        StudentTest st21 = new StudentTest();
        st21.setTest(test5);
        st21.setStudent(student6);
        st21.setScore(80.4); 
        try 
        {  
            String datestr="27/04/2016";
            String datestr2="25/04/2016";
            String datestr3="10/02/2016";
            String datestr4="07/05/2016";
            String datestr5="13/03/2016";
            String datestr6="17/02/2016";
            String datestr7="01/03/2016";
            DateFormat formatter; 
            formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date1 = (Date)formatter.parse(datestr); 
            Date date2 = (Date)formatter.parse(datestr2);
            Date date3 = (Date)formatter.parse(datestr3);
            Date date4 = (Date)formatter.parse(datestr4);
            Date date5 = (Date)formatter.parse(datestr5);
            Date date6 = (Date)formatter.parse(datestr6);
            Date date7 = (Date)formatter.parse(datestr7);
            st.setDatum(date1); 
            st2.setDatum(date1);
            st9.setDatum(date1);
            st3.setDatum(date3);            
            st4.setDatum(date3);
            st5.setDatum(date3); 
            st6.setDatum(date4);
            st7.setDatum(date4);
            st8.setDatum(date4);
            st10.setDatum(date2);
            st11.setDatum(date2);
            st12.setDatum(date2);
            st13.setDatum(date5);
            st14.setDatum(date5);
            st15.setDatum(date5);
            st16.setDatum(date6);
            st17.setDatum(date6);
            st18.setDatum(date6);
            st19.setDatum(date7);
            st20.setDatum(date7);
            st21.setDatum(date7);
        } 
            catch (Exception e)
        {}
        studentTesten.add(st);
        studentTesten.add(st2);
        studentTesten.add(st3);
        studentTesten.add(st4);
        studentTesten.add(st5);   
        studentTesten.add(st6); 
        studentTesten.add(st7); 
        studentTesten.add(st8); 
        studentTesten.add(st9); 
        studentTesten.add(st10); 
        studentTesten.add(st11); 
        studentTesten.add(st12); 
        studentTesten.add(st13); 
        studentTesten.add(st14); 
        studentTesten.add(st15); 
        studentTesten.add(st16); 
        studentTesten.add(st17); 
        studentTesten.add(st18); 
        studentTesten.add(st19); 
        studentTesten.add(st20); 
        studentTesten.add(st21); 
        studentTestService.saveStudentTestken(studentTesten);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
