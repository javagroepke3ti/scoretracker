/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.beans.StudentService;
import be.thomasmore.java.beans.StudentTestService;
import be.thomasmore.java.entity.Student;
import be.thomasmore.java.entity.StudentTest;
import be.thomasmore.java.entity.Vak;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JFileChooser;

/**
 *
 * @author e_for
 */
@MultipartConfig()
@WebServlet(name = "WebService3Servlet", urlPatterns = {"/WebService3Servlet"})
public class WebService3Servlet extends HttpServlet {

    @EJB
    private StudentService studentService;
    @EJB
    private StudentTestService studentTestService;
    
    JFileChooser chooser;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = null;
        
        if (request.getParameter("rapport") != null)
        {
            if (request.getParameter("gekozenStudent").equals("keuze") || request.getParameter("datepicker").equals("") || request.getParameter("datepicker2").equals(""))
            {
                if (request.getParameter("gekozenStudent").equals("keuze"))
                {
                    request.setAttribute("foutStudent", "Kies een student!");
                }
                if (request.getParameter("datepicker").equals(""))
                {
                    request.setAttribute("foutStartDatum", "Kies een startdatum!");
                }
                if (request.getParameter("datepicker2").equals(""))
                {
                    request.setAttribute("foutEindDatum", "Kies een einddatum!");
                }
                Long klasId = Long.parseLong(request.getParameter("klasId"));
                List<Student>studenten = studentService.findAllStudentsPerKlas(klasId);
                request.setAttribute("studenten", studenten);
                request.setAttribute("keuze", "student");               
                rd = request.getRequestDispatcher("rapport_student.jsp"); 
            }
            else
            {
               try 
                {  
                    response.setHeader("Expires", "0");
                    response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                    response.setHeader("Pragma", "public");
                    response.setContentType("application/pdf");
                    response.addHeader("Content-Disposition", "attachment; filename=Rapport.pdf");

                    Long studentId = Long.parseLong(request.getParameter("gekozenStudent"));

                    String startDatum = request.getParameter("datepicker");
                    String eindDatum = request.getParameter("datepicker2");

                    createPdf(studentId, startDatum, eindDatum, response);
                    
                    
                } 
                    catch (Exception e)
                {} 
            }
            
        }         
        rd.forward(request, response);   
    }
    
    public void createPdf(long studentId, String startDatum, String eindDatum, HttpServletResponse response) throws IOException, ParseException {

        DecimalFormat df = new DecimalFormat("0.00");

        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        
        DateFormat formatter; 
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = (java.util.Date)formatter.parse(startDatum); 
        Date date2 = (java.util.Date)formatter.parse(eindDatum);

        //Student ophalen adhv zijn/haar ID
        //Nadien ophalen van zijn/haar vakken en de testen tussen twee data
        Student student = studentService.findStudentById(studentId);
        List<Vak>vakken = student.getVakken();
        List<StudentTest> testen = studentTestService.findTestsPerStudentAndDate(studentId, date1, date2);

        //PDF aanmaken
        OutputStream pdfFileout = response.getOutputStream();

        PdfWriter writer = new PdfWriter(pdfFileout);

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);

        //Document aanmaken
        Document document = new Document(pdf);
        {
            document.add(new Paragraph(
                "Rapport: " + startDatum + " - " + eindDatum + "\n"
                + "Naam: " + student.getVoornaam() + " " + student.getNaam() + "\n"
                + "Klas: " + student.getKlas() + "\n"
            ));

            Table table = new Table(new float[]{4, 4, 3, 3});
            table.setBorderTop(new SolidBorder(1));
            table.setBorderBottom(new SolidBorder(1));
            table.setBorderLeft(new SolidBorder(1));
            table.setBorderRight(new SolidBorder(1));
            process(table, "Vak;Punten;Klasgemiddelde; Mediaan", bold, Color.LIGHT_GRAY);
            table.setWidthPercent(100);
            document.add(new Paragraph());

            int teller = 0;

            for(Vak vak: vakken)
            {
                teller++;
                List<StudentTest>testenPerVak = studentTestService.findAllTestsPerStudentByVakAndDate(student.getId(), vak.getId(), date1, date2);

                if (testenPerVak.size() > 0)
                {
                    //Score student berekenen
                    double totaleScore = 0.0;
                    double scoreStudent;

                    for (StudentTest test : testenPerVak)
                    {
                        totaleScore += test.getScore();
                    }
                    scoreStudent = totaleScore / testenPerVak.size();

                    //Gemiddelde en mediaan van de klas berekenen
                    List<StudentTest>testenPerKlas = studentTestService.findAllTestsFromKlasByVakAndDate(student.getKlas().getId(), vak.getId(), date1, date2);
                    List<Double>scores = new ArrayList<Double>();

                    double totaleScoreKlas = 0.0;
                    double gemiddeldeKlas;

                    for (StudentTest testKlas : testenPerKlas)
                    {
                        totaleScoreKlas += testKlas.getScore();
                        scores.add(testKlas.getScore());
                    }

                    gemiddeldeKlas = totaleScoreKlas / testenPerKlas.size();

                    Collections.sort(scores);
                    int lengte = scores.size();
                    double mediaan = scores.get(lengte/2);
                             
                    if (teller % 2 == 0)
                    {
                        process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.WHITE);
                    }
                    else
                    {
                        process(table, vak.getNaam() + ";" + String.format("%.2f", scoreStudent) + "%;" + String.format("%.2f", gemiddeldeKlas) + "%;" + String.format("%.2f", mediaan) + "%", font, Color.CYAN);
                    }

                 }
            }
            document.add(table);
            document.close();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.writeTo(pdfFileout);

        byte[] buffer = new byte[4096];
        int length;
        while ((length = buffer.length) > 0) {
            pdfFileout.write(buffer, 0, length);
        }

        writer.flush();
        writer.close();
    }
    
    public void process(Table table, String line, PdfFont font, Color backgroundColor) {
        StringTokenizer tokenizer = new StringTokenizer(line, ";");
        while (tokenizer.hasMoreTokens()) {

            table.addCell(
                    new Cell().add(
                            new Paragraph(tokenizer.nextToken()).setFont(font).setMarginLeft(1)).setBorder(Border.NO_BORDER).setPadding(0.5f).setPaddingRight(0.5f).setBorderLeft(new SolidBorder(1)).setBackgroundColor(backgroundColor));

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
