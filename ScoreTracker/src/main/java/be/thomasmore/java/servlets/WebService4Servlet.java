package be.thomasmore.java.servlets;

import be.thomasmore.java.beans.DownloadTestenPdfService;
import be.thomasmore.java.beans.KlasService;
import be.thomasmore.java.entity.Klas;
import be.thomasmore.java.entity.Test;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JFileChooser;


/**
 *
 * @author e_for
 */
@MultipartConfig()
@WebServlet(name = "WebService4Servlet", urlPatterns = {"/WebService4Servlet"})
public class WebService4Servlet extends HttpServlet {

    @EJB
    private KlasService klasService;
    @EJB
    private DownloadTestenPdfService pdfService;

    Klas gekozenklas;
    Test gekozenTest;

    JFileChooser chooser;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = null;

        if (request.getParameter("maakPDF") != null) {
            Long klasId = Long.parseLong(request.getParameter("klasId"));
            Long testId = Long.parseLong(request.getParameter("testen"));

            if (request.getParameter("testen").equals("keuze")) {
                Klas klas = klasService.findKlasById(klasId);
                List<Test> testenPerKlas = klas.getTesten();
                request.setAttribute("testen", testenPerKlas);
                request.setAttribute("keuze", "test");
                request.setAttribute("klasId", klasId);
                rd = request.getRequestDispatcher("rapport_opdracht.jsp");
                request.setAttribute("foutTest", "Kies een test!");
            } else {
                pdfService.createPdf(klasId, testId, response);
            }
        }
        rd.forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
