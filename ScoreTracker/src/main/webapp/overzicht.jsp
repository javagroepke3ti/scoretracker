<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ScoreTracker | Overzicht</title>
    <link href="plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/main-style.css" rel="stylesheet" />
    <!-- Page-Level CSS -->
    <link href="plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
   </head>
<body>

        <!--Menu aan de zijkant-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <!--Gebruikersinfo-->
                    <li>
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="img/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div>Voornaam <strong>Naam</strong></div>
                            </div>
                        </div>
                    </li>
                    <!--Menu-items-->
                    <li class="selected listItem">
                        <span class="menuItem"><a href="InlogServlet?inloggen=Y">Overzicht</a></span>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Klassen</span>
                        <ul>
                            <li><a href="upload.jsp">Nieuwe klassenlijst uploaden</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Opdrachten</span>
                        <ul>
                            <li><a href="upload_opdracht.jsp">Nieuwe opdracht uploaden</a></li>
                        </ul>
                    </li>
                    <li class="listItem">
                        <span class="menuItem">Rapporten</span>
                        <ul>
                            <li><a href="KeuzeServlet?rapportOpdracht=Y">Rapport per opdracht</a></li>
                            <li><a href="KeuzeServlet?rapportStudent=Y">Rapport per student</a></li>
                        </ul>
                    </li> 
                    <li class="listItem">
                        <span class="menuItem">Log uit</span>
                    </li>
                </ul>
            </div>
        </nav>
        
        <!--Groot grijs middenstuk-->
        <div id="page-wrapper">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overzicht</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h3>Klassen</h3>
                    <ul>
                        <li><a href="upload.jsp">Nieuwe klassenlijst uploaden</a></li>
                    </ul>
                    
                    <h3>Opdrachten</h3>
                    <ul>
                            <li><a href="upload_opdracht.jsp">Nieuwe opdracht uploaden</a></li>
                    </ul>
                    
                    <h3>Rapporten</h3>
                    <ul>
                        <li><a href="KeuzeServlet?rapportOpdracht=Y">Rapport per opdracht</a></li>
                        <li><a href="KeuzeServlet?rapportStudent=Y">Rapport per student</a></li>
                    </ul>
                </div>
            </div>
        </div>


    <!-- Core Scripts - Include with every page -->
    <script src="assets/plugins/jquery-1.10.2.js"></script>
    <script src="assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/plugins/pace/pace.js"></script>
    <script src="assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    <script src="assets/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/plugins/morris/morris.js"></script>
    <script src="assets/scripts/dashboard-demo.js"></script>

</body>

</html>
