<%-- 
    Document   : test
    Created on : 17-okt-2016, 9:33:15
    Author     : e_for
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Testing one two</title>
    </head>
    <body>
        <h1>Alle vakken van ${student.getVoornaam()} ${student.getNaam()}:</h1>
        <ul>
          <c:forEach var="vak" items="${vakken}">
              <li>${vak.getNaam()}</li>
          </c:forEach>  
        </ul>     
        
        <h1>Alle testen van ${student.getVoornaam()} ${student.getNaam()}:</h1>
        <ul>
          <c:forEach var="test" items="${testen}">
              <li>${test.getTest().getNaam()} - ${test.getTest().getVak().getNaam()} : ${test.getScore()}</li>
          </c:forEach>  
        </ul> 
    </body>
</html>
