<%-- 
    Document   : Thomas
    Created on : 14-okt-2016, 23:12:51
    Author     : e_for
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8 ">
        <title>PDF met Test resultaten</title>
    </head>
    <body>
        <h1>Op deze pagina kan men een test selecteren en hiervan de resultaten in een pdf downloaden</h1>
        <p>
            <form action="MailServlet" method="POST"  enctype='multipart/form-data'>
                Bestand:
                <input type="file" name="rapport" id="rapport" accept="application/pdf"/> <br/>
                <input type="submit" value="Upload" name="upload" id="upload"/>
            </form>
    </p>
</body>
</html>
