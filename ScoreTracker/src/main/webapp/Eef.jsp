<%-- 
    Document   : Eef
    Created on : 14-okt-2016, 23:13:05
    Author     : e_for
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
        <script>
        $(function() {
            $( "#datepicker" ).datepicker({dateFormat: 'dd/mm/yy'});
            $( "#datepicker2" ).datepicker({dateFormat: 'dd/mm/yy'});
        });
        </script>
        <title>Eef</title>
    </head>
    <body>
        <h1>Opvragen van een rapport van een bepaalde student in een bepaalde tijdspanne:</h1>
        <form action="WebService3Servlet" method="get">
        <p>Kies hier de student waarvan je het rapport wilt opvragen:</p>
        <select name="gekozenStudent">
            <c:forEach var="student" items="${studenten}">
                <option value="${student.getId()}">${student.getNaam()} ${student.getVoornaam()}</option>
            </c:forEach>
        </select>
        
        <p>Kies hier de begindatum:</p>
        <input type="text" id="datepicker" name="datepicker"/>
        
        <p>En de einddatum:</p>
        <input type="text" id="datepicker2" name="datepicker2"/>
        
        <p><input type="submit" value="Ophalen dat rapport!" name="rapport"/></p>    
        </form>
    </body>
</html>
