/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.servlets;

import be.thomasmore.java.beans.StudentService;
import be.thomasmore.java.entity.Student;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Thomas
 */
@WebServlet(name = "Opdracht2Servlet", urlPatterns = {"/Opdracht2Servlet"})
@MultipartConfig()
public class Opdracht2Servlet extends HttpServlet {
    
    @EJB
    private StudentService studentService;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = null;
        try {
            Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
            FileInputStream file = (FileInputStream)filePart.getInputStream();
            
            List<Student> studenten = studentService.readStudentsFromFileAndSave(file);
            
            request.setAttribute("studenten", studenten);
            
            rd = request.getRequestDispatcher("resultaat.jsp");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {

        }
        
        rd.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
