/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.java.excel;

import be.thomasmore.java.entity.Student;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Thomas
 */
public interface ExcelReader {
    public List<Student> readStudents(FileInputStream file);
}
